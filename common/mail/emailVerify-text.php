<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $password string */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?= $user->email ?>,

Your password <?= $password ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>
