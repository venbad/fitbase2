<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * UserCounter model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $counter
 * @property integer $updated_at
 *
 * @property User    $user
 */
class UserCounter extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_counter}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
              'class' => TimestampBehavior::className(),
              'createdAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'counter', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user' => 'Пользователь',
            'user.email' => 'E-mail пользователя',
            'counter' => 'Счетчик',
            'updated_at' => 'Дата последнего клика'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Проверка первый ли пользователь в рейтинге
     *
     * @param int $userId
     * @return bool
     */
    public static function checkFirst(int $userId)
    {
        return (int)static::find()
                ->select('user_id')
                ->orderBy(['counter' => SORT_DESC, 'updated_at' => SORT_ASC])
                ->scalar() === (int)$userId;
    }

    /**
     * Список кроме текущего пользователя
     *
     * @param int $userId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function allWithoutCurrent(int $userId)
    {
        return static::find()->where(['!=', 'user_id', $userId])->all();
    }
}
