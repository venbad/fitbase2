<?php

namespace frontend\controllers;

use common\models\UserCounter;
use common\models\LoginForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Counter controller
 */
class CounterController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => UserCounter::allWithoutCurrent($model->getUser()->id),
                'pagination' => false
            ]);

            return $this->render('index', [
                'first' => UserCounter::checkFirst($model->getUser()->id),
                'counter' => $model->getUser()->userCounter->counter,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('form', [
            'model' => $model
        ]);
    }

}
