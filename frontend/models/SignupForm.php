<?php
namespace frontend\models;

use common\models\UserCounter;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $birth_date;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['birth_date', 'string'],
            ['birth_date', 'required', 'message' => 'Необходимо заполнить дату рождения'],
            [['birth_date'], function ($attribute, $params, $validator) {
                $date = date_parse_from_format("d.m.Y", $this->birth_date);
                if (!checkdate($date['month'], $date['day'], $date['year'])) {
                    return $this->addError($attribute, 'Некорректный формат даты');
                }

                $birthDate = new \DateTime($this->birth_date);

                $now = new \DateTime();

                $age = $now->diff($birthDate);

                if ($age->y < 16) {
                    $this->addError($attribute, 'Too young!');
                } elseif ($age->y > 150) {
                    $this->addError($attribute, 'Too old!');
                }
            }],
            
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Необходимо заполнить адрес E-mail'],
            ['email', 'email', 'message' => 'Некорректный E-mail адрес'],
            ['email', 'string', 'max' => 255, 'tooLong' => 'Максимальная длина адреса 255 символов'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail адрес уже занят'],

            ['password', 'required', 'message' => 'Необходимо заполнить пароль'],
            ['password', 'string', 'min' => 6, 'tooShort' => 'Минимальная длина пароля 6 символов'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'birth_date' => 'Дата рождения'
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save() && $this->sendEmail($user, $this->password);

    }

    /**
     * @return bool|null
     */
    public function afterSignup()
    {
        $userCounter = new UserCounter();
        $userCounter->user_id = User::find()
            ->select('id')
            ->where(['email' => $this->email])
            ->scalar();
        return $userCounter->save();

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @param string $password
     * @return bool whether the email was sent
     */
    protected function sendEmail($user, $password = null)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user, 'password' => $password]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
