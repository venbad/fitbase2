<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Fitbase';
?>

<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <?php if (\Yii::$app->user->isGuest) : ?>
                    <a class="btn btn-primary" href="<?php echo Url::to(['/site/signup']) ?>">Зарегистрироваться</a>
                <?php else : ?>
                    <?php Pjax::begin([
                        'enablePushState' => false,
                        'linkSelector' => '.pjax-trigger'
                    ])?>
                    <?php echo $model->counter ?>
                    <?php Pjax::end() ?>
                <?php endif ?>
            </div>
            <div class="col-lg-4">
                <?php if (\Yii::$app->user->isGuest) : ?>
                    <a class="btn btn-primary" href="<?php echo Url::to(['/site/login']) ?>">Войти</a>
                <?php else : ?>
                    <a class="btn btn-success pjax-trigger" href="<?php Url::to(['site/index'])?>">+1</a>
                <?php endif ?>
            </div>
            <div class="col-lg-4">
                <?php if (\Yii::$app->user->isGuest) : ?>
                    <a class="btn btn-primary" href="<?php echo Url::to(['/counter/index']) ?>">Счетчик</a>
                <?php else : ?>
                    <?php echo Html::a('Выйти', Url::to(['/site/logout']), ['data-method' => 'POST', 'class' => 'btn btn-primary']) ?>
                <?php endif ?>
            </div>
        </div>

    </div>
</div>
