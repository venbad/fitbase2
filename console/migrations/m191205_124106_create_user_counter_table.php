<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_counter}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m191205_124106_create_user_counter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_counter}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unique()->comment('Id пользователя'),
            'counter' => $this->integer()->defaultValue(0)->comment('Счетчик'),
            'updated_at' => $this->integer()->comment('Дата обновления'),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_counter-user_id}}',
            '{{%user_counter}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_counter-user_id}}',
            '{{%user_counter}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_counter-user_id}}',
            '{{%user_counter}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_counter-user_id}}',
            '{{%user_counter}}'
        );

        $this->dropTable('{{%user_counter}}');
    }
}
